package com.car;

import com.car.components.Engine;
import com.car.components.Wheels;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class CarTest {

    private Car car;

    @BeforeEach
    public void createCar() {
        Engine engine = new Engine();
        Wheels wheels = new Wheels();
        car = new Car(engine, wheels);
    }

    @Test
    public void carMovement() {
        car.steerForward();
        car.insertKey();
        car.pressGas();
        car.turnKey();
        car.pressGas();
        car.putCarInDrive();
        car.pressGas();
        car.pressGas();
        car.steerLeft();
        car.steerRight();
        car.putCarInPark();
        car.pressBrake();
        car.pressBrake();
        car.putCarInPark();
        car.putCarInReverse();
        car.pressGas();
        car.pressBrake();
        car.putCarInPark();
        car.turnKey();
        car.removeKey();
        car.pressGas();
    }
}
