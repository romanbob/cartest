package com.car.components;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Wheels {

    public static final byte STEP = 10;
    public static final byte MAX_ANGLE = 90;

    // left: -90 < angel < 0
    // forward: angel = 0
    // left: 0 < angel < 90
    private byte angle = 0;
}
