package com.car.components;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Engine {

    public static final int STEP = 5;

    private int speed;
    private ENGINE_MODE engineMode = ENGINE_MODE.OFF;

    public enum ENGINE_MODE {
        ON, OFF
    }
}
