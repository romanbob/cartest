package com.car.states;

import com.car.Car;
import com.car.components.Engine;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class InsertedKeyState implements CarState {

    private Car car;

    public InsertedKeyState(Car car) {
        this.car = car;
    }

    @Override
    public void insertKey() {
        log.info("INSERTED KEY STATE. Insert key. Key already inserted.");
    }

    @Override
    public void removeKey() {
        log.info("INSERTED KEY STATE. Remove key. Removing key...");
        car.setState(car.getInitState());
        car.setKeyMode(Car.KEY_MODE.REMOVED);
    }

    @Override
    public void turnKey() {
        log.info("INSERTED KEY STATE. Turning key...");
        car.setState(car.getParkModeState());
        car.getEngine().setEngineMode(Engine.ENGINE_MODE.ON);
        car.setKeyMode(Car.KEY_MODE.TURNED);
    }

    @Override
    public void pressBrake() {
        log.info("INSERTED KEY STATE. Press Brake. Brake is blocked. Turn on engine first.");
    }

    @Override
    public void releaseBrake() {
        log.info("INSERTED KEY STATE. Release Brake. Brake is blocked. Turn on engine first.");
    }

    @Override
    public void pressGas() {
        log.info("INSERTED KEY STATE. Press Gas. Gas is blocked. Turn on engine first.");
    }

    @Override
    public void releaseGas() {
        log.info("INSERTED KEY STATE. Release Gas. Gas is blocked. Turn on engine first.");
    }

    @Override
    public void steerLeft() {
        log.info("INSERTED KEY STATE. Steer Left. Steer is blocked. Turn on engine first.");
    }

    @Override
    public void steerForward() {
        log.info("INSERTED KEY STATE. Steer Forward. Steer is blocked. Turn on engine first.");
    }

    @Override
    public void steerRight() {
        log.info("INSERTED KEY STATE. Steer Right. Steer is blocked. Turn on engine first.");
    }

    @Override
    public void putCarInPark() {
        log.info("INSERTED KEY STATE. Put car in park. Gearstick is blocked. Turn on engine first.");
    }

    @Override
    public void putCarInDrive() {
        log.info("INSERTED KEY STATE. Put car in drive. Gearstick is blocked. Turn on engine first.");
    }

    @Override
    public void putCarInReverse() {
        log.info("INSERTED KEY STATE. Put car in reverse. Gearstick is blocked. Turn on engine first.");
    }
}
