package com.car.states;

import com.car.Car;
import com.car.components.Engine;
import com.car.components.Wheels;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DriveModeState implements CarState {

    private Car car;

    public DriveModeState(Car car) {
        this.car = car;
    }

    @Override
    public void insertKey() {
        log.info("DRIVE MODE STATE. DRIVE MODE STATE. Insert key. Key already inserted.");
    }

    @Override
    public void removeKey() {
        log.info("DRIVE MODE STATE. Remove key. Switch to the Park Mode first.");
    }

    @Override
    public void turnKey() {
        log.info("DRIVE MODE STATE. Turn key. Switch to the Park Mode first.");
    }

    @Override
    public void pressBrake() {
        Engine engine = car.getEngine();
        int speed = engine.getSpeed();
        if (speed > 0) {
            speed -= Engine.STEP;
            engine.setSpeed(speed);
        }

        log.info("DRIVE MODE STATE. Press Brake (speed={})...", speed);
    }

    @Override
    public void releaseBrake() {
        log.info("DRIVE MODE STATE. Releasing Brake ...");
    }

    @Override
    public void pressGas() {
        Engine engine = car.getEngine();
        int speed = engine.getSpeed();
        if (speed < Car.MAX_SPEED) {
            speed += Engine.STEP;
            engine.setSpeed(speed);
        }

        log.info("DRIVE MODE STATE. Pressing Gas (speed={})...", speed);
    }

    @Override
    public void releaseGas() {
        log.info("DRIVE MODE STATE. Releasing Gas...");
    }

    @Override
    public void steerLeft() {
        Wheels wheels = car.getWheels();
        byte angle = wheels.getAngle();
        if (Math.abs(angle) < Wheels.MAX_ANGLE) {
            angle -= Wheels.STEP;
            wheels.setAngle(angle);
        }

        log.info("DRIVE MODE STATE. Steering Left (angle={})...", angle);
    }

    @Override
    public void steerForward() {
        Wheels wheels = car.getWheels();
        wheels.setAngle((byte) 0);

        log.info("DRIVE MODE STATE. Steering Forward (angle=0)...");
    }

    @Override
    public void steerRight() {
        Wheels wheels = car.getWheels();
        byte angle = wheels.getAngle();
        if (angle < Wheels.MAX_ANGLE) {
            angle += Wheels.STEP;
            wheels.setAngle(angle);
        }

        log.info("DRIVE MODE STATE. Steering Right (angle={})...", angle);
    }

    @Override
    public void putCarInPark() {
        Engine engine = car.getEngine();
        int speed = engine.getSpeed();
        if (speed == 0) {
            log.info("DRIVE MODE STATE. Switching to Park Mode...");
            car.setState(car.getParkModeState());
        } else {
            log.info("DRIVE MODE STATE. Put car in park. Switching to Park Mode is not possible. Speed is {} (should be 0).", speed);
        }
    }

    @Override
    public void putCarInDrive() {
        log.info("DRIVE MODE STATE. Put car in drive. Already in Drive Mode.");
    }

    @Override
    public void putCarInReverse() {
        log.info("DRIVE MODE STATE. Put car in reverse. Switch first to Park Mode.");
    }
}
