package com.car.states;

import com.car.Car;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class InitState implements CarState {

    Car car;

    public InitState(Car car) {
        this.car = car;
    }

    @Override
    public void insertKey() {
        log.info("INIT STATE. Inserting key...");
        car.setState(car.getInsertedKeyState());
        car.setKeyMode(Car.KEY_MODE.INSERTED);
    }

    @Override
    public void removeKey() {
        log.info("INIT STATE. Remove key. There is no key.");
    }

    @Override
    public void turnKey() {
        log.info("INIT STATE. Turn key. There is no key.");
    }

    @Override
    public void pressBrake() {
        log.info("INIT STATE. Press Brake. There is no key. Brake pedal is blocked.");
    }

    @Override
    public void releaseBrake() {
        log.info("INIT STATE. Release Brake. There is no key. Brake pedal is blocked.");
    }

    @Override
    public void pressGas() {
        log.info("INIT STATE. Press Gas. There is no key. Gas pedal is blocked.");
    }

    @Override
    public void releaseGas() {
        log.info("INIT STATE. Release Gas. There is no key. Gas pedal is blocked.");
    }

    @Override
    public void steerLeft() {
        log.info("INIT STATE. Steer Left. There is no key. Steer is blocked.");
    }

    @Override
    public void steerForward() {
        log.info("INIT STATE. Steer Forward. There is no key. Steer is blocked.");
    }

    @Override
    public void steerRight() {
        log.info("INIT STATE. Steer Right. There is no key. Steer is blocked.");
    }

    @Override
    public void putCarInPark() {
        log.info("INIT STATE. Put car in park. There is no key. Gearstick is blocked.");
    }

    @Override
    public void putCarInDrive() {
        log.info("INIT STATE. Put car in drive. There is no key. Gearstick is blocked.");
    }

    @Override
    public void putCarInReverse() {
        log.info("INIT STATE. Put car in reverse. There is no key. Gearstick is blocked.");
    }
}
