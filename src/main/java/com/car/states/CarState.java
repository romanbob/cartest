package com.car.states;

public interface CarState {

    void insertKey();
    void removeKey();
    void turnKey();

    void pressBrake();
    void releaseBrake();

    void pressGas();
    void releaseGas();

    void steerLeft();
    void steerForward();
    void steerRight();

    void putCarInPark();
    void putCarInDrive();
    void putCarInReverse();
}
