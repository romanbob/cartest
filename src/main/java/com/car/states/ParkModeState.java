package com.car.states;

import com.car.Car;
import com.car.components.Engine;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ParkModeState implements CarState {

    private Car car;

    public ParkModeState(Car car) {
        this.car = car;
    }

    @Override
    public void insertKey() {
        log.info("PARK MODE STATE. Insert Key. Key already inserted.");
    }

    @Override
    public void removeKey() {
        log.info("PARK MODE STATE. Remove key. First turn key back.");
    }

    @Override
    public void turnKey() {
        log.info("PARK MODE STATE. Turn key. Turning key back. Stopping engine...");
        car.setState(car.getInsertedKeyState());
        car.getEngine().setEngineMode(Engine.ENGINE_MODE.OFF);
    }

    @Override
    public void pressBrake() {
        log.info("PARK MODE STATE. Press Brake. Car is not moving.");
    }

    @Override
    public void releaseBrake() {
        log.info("PARK MODE STATE. Release Brake. Car is not moving.");
    }

    @Override
    public void pressGas() {
        log.info("PARK MODE STATE. Press Gas. Car is not moving.");
    }

    @Override
    public void releaseGas() {
        log.info("PARK MODE STATE. Release Gas. Car is not moving.");
    }

    @Override
    public void steerLeft() {
        log.info("PARK MODE STATE. Steer Left. Car is not moving.");
    }

    @Override
    public void steerForward() {
        log.info("PARK MODE STATE. Steer Forward. Car is not moving.");
    }

    @Override
    public void steerRight() {
        log.info("PARK MODE STATE. Steer Right. Car is not moving.");
    }

    @Override
    public void putCarInPark() {
        log.info("PARK MODE STATE. Put car in park. Already in Park mode");
    }

    @Override
    public void putCarInDrive() {
        log.info("PARK MODE STATE. Put car in drive. Switching to Drive Mode ...");
        car.setState(car.getDriveModeState());
    }

    @Override
    public void putCarInReverse() {
        log.info("PARK MODE STATE. Put car in reverse. Switching to Reverse Mode ...");
        car.setState(car.getReverseModeState());
    }
}
