package com.car;

import com.car.components.Engine;
import com.car.components.Wheels;
import com.car.states.CarState;
import com.car.states.DriveModeState;
import com.car.states.InitState;
import com.car.states.InsertedKeyState;
import com.car.states.ParkModeState;
import com.car.states.ReverseModeState;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Car implements CarState {

    public static final int MAX_SPEED = 150;

    @Getter
    private Engine engine;

    @Getter
    private Wheels wheels;
    @Setter
    private KEY_MODE keyMode = KEY_MODE.REMOVED;

    @Setter
    private CarState state;
    @Getter
    private CarState initState = new InitState(this);
    @Getter
    private CarState insertedKeyState = new InsertedKeyState(this);
    @Getter
    private CarState parkModeState = new ParkModeState(this);
    @Getter
    private CarState driveModeState = new DriveModeState(this);
    @Getter
    private CarState reverseModeState = new ReverseModeState(this);

    public Car(Engine engine, Wheels wheels) {
        log.info("The car is off");
        this.engine = engine;
        this.wheels = wheels;
        this.state = initState;
    }

    public enum KEY_MODE {
        REMOVED, INSERTED, TURNED
    }

    @Override
    public void insertKey() {
        state.insertKey();
    }

    @Override
    public void removeKey() {
        state.removeKey();
    }

    @Override
    public void turnKey() {
        state.turnKey();
    }

    @Override
    public void pressBrake() {
        state.pressBrake();
    }

    @Override
    public void releaseBrake() {
        state.releaseBrake();
    }

    @Override
    public void pressGas() {
        state.pressGas();
    }

    @Override
    public void releaseGas() {
        state.releaseGas();
    }

    @Override
    public void steerLeft() {
        state.steerLeft();
    }

    @Override
    public void steerForward() {
        state.steerForward();
    }

    @Override
    public void steerRight() {
        state.steerRight();
    }

    @Override
    public void putCarInPark() {
        state.putCarInPark();
    }

    @Override
    public void putCarInDrive() {
        state.putCarInDrive();
    }

    @Override
    public void putCarInReverse() {
        state.putCarInReverse();
    }
}
